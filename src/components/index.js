import mAppShell from './packages/m-app-shell/index.js'
import mFloor from './packages/m-floor/index.js'
import mGoodsBox from './packages/m-goods-box/index.js'
import mCategoryBox from './packages/m-category-box/index.js'
import mCategoryItem from './packages/m-category-item/index.js'
import mLoginBar from './packages/m-login-bar/index.js'
import mButton from './packages/m-button/index.js'
import mGoodsGroup from './packages/m-goods-group/index.js'
import mCell from './packages/m-cell/index.js'
import mSheetGroup from './packages/m-sheet-group/index.js'
import mTimePicker from './packages/m-time-picker/index.js'
import mToast from './packages/m-toast/index.js'
import mSlideNav from './packages/m-slider-nav/index.js'
import mIndicator from './packages/m-indicator/index.js'
import mImgUploader from './packages/m-img-uploader/index.js'
import mInput from './packages/m-input/index.js'
import mPop from './packages/m-pop/index.js'
import mAddressPicker from './packages/m-address-picker/index.js'
import mAddressCell from './packages/m-address-cell/index.js'
import mSelect from './packages/m-select/index.js'
import mCartCell from './packages/m-cart-cell/index.js'
import mNumber from './packages/m-number/index.js'
import mSpecialCell from './packages/m-special-cell/index.js'
import mLoaderMore from './packages/m-load-more/index.js'
import mEvaluateCell from './packages/m-evaluate-cell/index.js'
import mGoodsCell from './packages/m-goods-cell/index.js'
import mOrderCell from './packages/m-order-cell/index.js'

const version = '1.0.0'

const install = function (Vue, config = {}) {
    if (install.installed) return

    Vue.component(mAppShell.name, mAppShell)
    Vue.component(mFloor.name, mFloor)
    Vue.component(mGoodsBox.name, mGoodsBox)
    Vue.component(mCategoryBox.name, mCategoryBox)
    Vue.component(mLoginBar.name, mLoginBar)
    Vue.component(mButton.name, mButton)
    Vue.component(mGoodsGroup.name, mGoodsGroup)
    Vue.component(mCell.name, mCell)
    Vue.component(mSheetGroup.name, mSheetGroup)
    Vue.component(mTimePicker.name, mTimePicker)
    Vue.component(mSlideNav.name, mSlideNav)
    Vue.component(mImgUploader.name, mImgUploader)
    Vue.component(mInput.name, mInput)
    Vue.component(mPop.name, mPop)
    Vue.component(mAddressPicker.name, mAddressPicker)
    Vue.component(mAddressCell.name, mAddressCell)
    Vue.component(mSelect.name, mSelect)
    Vue.component(mCartCell.name, mCartCell)
    Vue.component(mNumber.name, mNumber)
    Vue.component(mSpecialCell.name, mSpecialCell)
    Vue.component(mCategoryItem.name, mCategoryItem)
    Vue.component(mLoaderMore.name, mLoaderMore)
    Vue.component(mEvaluateCell.name, mEvaluateCell)
	Vue.component(mGoodsCell.name, mGoodsCell)
	Vue.component(mOrderCell.name, mOrderCell)

    Vue.$mToast = Vue.prototype.$mToast = mToast;
    Vue.$mIndicator = Vue.prototype.$mIndicator = mIndicator;
};

export default {
    version,
    install
}