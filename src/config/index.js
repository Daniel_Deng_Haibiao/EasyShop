export default {
    api: function () {
        return this.mock.status ? '//127.0.0.1/mock' : (process.env.NODE_ENV === 'development' ? '//127.0.0.1:8010' : '//api.neho.top')
    },
    wx: {
        appid: 'wx010795ebc4bcc074',
        redirect_uri: 'http://mobile.neho.top/wxLogin',
        scope: 'snsapi_userinfo'
    },
    mock: {
        api:'//127.0.0.1/mock',
        status:false
    }
}