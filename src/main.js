// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import App from './App'
import {router} from './router'
import components from './components'
import myPlugin from './plugin'
import {ToastPlugin} from 'vux'
import './static/less/iconfont.less'
import store from './vuex'
import './service/mock-service/index'
import {isLogin} from "./utils/common";
import './utils/prototype'

Vue.use(ToastPlugin)
Vue.use(myPlugin)
Vue.use(components)
FastClick.attach(document.body)
Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
    router,
    store,
    render: h => h(App),
    created() {
        if (isLogin()) {
            this.getUserInfo();
        }
    },
    mounted: function () {
        /*
        *  调节不同设备的字体
        * */
        let html = document.getElementsByTagName("html")[0];
        let height = innerHeight;
        let width = innerWidth;
        var size;
        if (width < height) {
            size = width / 320 * 20;
        } else {
            size = height / 320 * 20;
        }
        html.style.fontSize = size + "px";
    }
}).$mount('#app-box');
