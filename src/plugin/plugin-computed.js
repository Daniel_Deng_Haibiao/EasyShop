import store from '../vuex'
import {isLogin} from '../utils/common'

export default {
    //用户信息
    userInfo: {
        get() {
            return this.$store.state.user.baseInfo
        }
    }
}