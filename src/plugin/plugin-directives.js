export default {
    bound: {
        inserted: function (el, binding) {
            let start = 0, distance = 0, end = 0;
            el.addEventListener('touchstart', e => {
                el.style.transition = '';
                switch (binding.value.type) {
                    case 'top':
                        start = e.touches[0].screenY;
                        break;
                    default:
                        break;
                }
            })
            el.addEventListener('touchmove', e => {
                switch (binding.value.type) {
                    case 'top':
                        if (Math.abs(distance) < binding.value.max) {
                            distance = e.touches[0].screenY - start;
                            el.style.transform = 'translateY(' + distance + 'px)';
                        }
                        break;
                    default:
                        break;
                }
            })
            el.addEventListener('touchend', e => {
                if (Math.abs(distance) > binding.value.max) {
                    binding.value.handler();
                }
                el.style.transition = 'all 0.3s';
                switch (binding.value.type) {
                    case 'top':
                        el.style.transform = 'translateY(0px)';
                        start = 0, distance = 0, end = 0;
                        break;
                    default:
                        break;
                }
            })
        }
    }
}