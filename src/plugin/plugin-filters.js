const moment = require('moment')
import config from '../config'

export default {
    priceFormat: function (value) {
        return '￥' + value;
    },
    timeStampFormat: function (value, arg) {
        return moment(value).format(arg);
    },
    staticSourceFilter: function (value) {
        return value ? (value.includes('http') ? value : config.api() + value) : require('../static/logo.png')
    },
    phoneFormat: function (value) {
        if (!value) return;
        const arrValue = value.split('');
        arrValue.forEach((i, k, array) => {
            if ((k < array.length - 4) && (k > 2)) {
                arrValue.splice(k, 1, '*')
            }
        })
        return arrValue.join('');
    },
}