import Vue from 'vue'
import VueRouter from 'vue-router'
import {isLogin} from "../utils/common"

Vue.use(VueRouter);

let routes = [
	{
		name: '/',
		path: '/',
		meta: {
			requiresAuth: false,
			title: 'index',
			level: 0
		},
		redirect: '/index',
		component: (resolve) => {
			require(['../view/index.vue'], resolve)
		}
	}, {
		name: 'login',
		path: '/login',
		meta: {
			requiresAuth: false,
			title: 'index',
			level: 0
		},
		component: (resolve) => {
			require(['../view/login.vue'], resolve)
		},
		beforeEnter: (to, from, next) => {
			if (isLogin()) {
				next('/index')
			}
			next();
		}
	}, {
		name: 'wxLogin',
		path: '/wxLogin',
		meta: {
			requiresAuth: false,
			title: 'index',
			level: 0
		},
		component: (resolve) => {
			require(['../view/wxLogin.vue'], resolve)
		}
	}, {
		name: 'regist',
		path: '/regist',
		meta: {
			requiresAuth: false,
			title: 'index',
			level: 0
		},
		component: (resolve) => {
			require(['../view/regist.vue'], resolve)
		},
		beforeEnter: (to, from, next) => {
			if (isLogin()) {
				next('/index')
			}
			next();
		}
	}, {
		name: 'complete',
		path: '/complete',
		meta: {
			requiresAuth: false,
			title: '资料完善',
			level: 0
		},
		component: (resolve) => {
			require(['../view/complete.vue'], resolve)
		},
	}, {
		name: 'goods',
		path: '/goods/:id',
		meta: {
			requiresAuth: false,
			title: '商品详情',
			level: 0
		},
		component: (resolve) => {
			require(['../view/goods/index'], resolve)
		}
	}, {
		name: 'index',
		path: '/index',
		meta: {
			requiresAuth: false,
			title: '首页',
			level: 2
		},
		component: (resolve) => {
			require(['../view/index/index'], resolve)
		}
	},
	{
		name: 'category',
		path: '/category',
		meta: {
			requiresAuth: false,
			title: '分类',
			level: 2
		},
		component: (resolve) => {
			require(['../view/category/index'], resolve)
		}
	},
	{
		name: 'find',
		path: '/find',
		meta: {
			requiresAuth: false,
			title: '发现',
			level: 2
		},
		component: (resolve) => {
			require(['../view/find/index'], resolve)
		}
	},
	{
		name: 'cart',
		path: '/cart',
		meta: {
			requiresAuth: false,
			title: '购物车',
			level: 2
		},
		component: (resolve) => {
			require(['../view/cart/index'], resolve)
		}
	},
	{
		name: 'center',
		path: '/center',
		meta: {
			requiresAuth: true,
			title: '个人中心',
			level: 2
		},
		component: (resolve) => {
			require(['../view/center/index'], resolve)
		}
	},
	{
		name: 'account',
		path: '/account',
		meta: {
			requiresAuth: true,
			title: '账户信息',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/account'], resolve)
		}
	},
	{
		name: 'account-links',
		path: '/account/links',
		meta: {
			requiresAuth: true,
			title: '账号关联',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/account-links'], resolve)
		}
	},
	{
		name: 'account-address',
		path: '/account/address',
		meta: {
			requiresAuth: true,
			title: '地址管理',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/account-address'], resolve)
		}
	},
	{
		name: 'address-add',
		path: '/address/add',
		meta: {
			requiresAuth: true,
			title: '添加地址',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/address-add'], resolve)
		}
	},
	{
		name: 'account-safety',
		path: '/account/safety',
		meta: {
			requiresAuth: true,
			title: '账户安全',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/account-safety'], resolve)
		}
	},
	{
		name: 'safety-modify',
		path: '/safety/modify',
		meta: {
			requiresAuth: true,
			title: '修改密码',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/safety-modify'], resolve)
		}
	},
	{
		name: 'profile',
		path: '/account/profile',
		meta: {
			requiresAuth: true,
			title: '个人信息',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/account-profile'], resolve)
		}
	},
	{
		name: 'avatar-avatar',
		path: '/profile/avatar',
		meta: {
			requiresAuth: true,
			title: '头像修改',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/profile-avatar'], resolve)
		}
	},
	{
		name: 'avatar-name',
		path: '/profile/name',
		meta: {
			requiresAuth: true,
			title: '名称修改',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/profile-name'], resolve)
		}
	},
	{
		name: 'order',
		path: '/order',
		meta: {
			requiresAuth: true,
			title: '我的订单',
			level: 1
		},
		component: (resolve) => {
			require(['../view/order/index'], resolve)
		}
	},
	{
		name: 'order-firm',
		path: '/order/firm',
		meta: {
			requiresAuth: true,
			title: '确认订单',
			level: 1
		},
		component: (resolve) => {
			require(['../view/order/order-firm'], resolve)
		}
	},
	{
		name: 'order-pay',
		path: '/order/pay',
		meta: {
			requiresAuth: true,
			title: '订单支付',
			level: 1
		},
		component: (resolve) => {
			require(['../view/order/order-pay'], resolve)
		}
	},
	{
		name: 'order-pay-status',
		path: '/order/pay/status',
		meta: {
			requiresAuth: true,
			title: '支付完成',
			level: 1
		},
		component: (resolve) => {
			require(['../view/order/order-pay-status'], resolve)
		}
	},
	{
		name: 'wallet',
		path: '/wallet',
		meta: {
			requiresAuth: true,
			title: '我的钱包',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/wallet'], resolve)
		}
	},
	{
		name: 'evaluate',
		path: '/evaluate',
		meta: {
			requiresAuth: true,
			title: '评价中心',
			level: 1
		},
		component: (resolve) => {
			require(['../view/center/evaluate'], resolve)
		}
	},
	{
		name: 'chat',
		path: '/chat',
		meta: {
			requiresAuth: true,
			title: '联系商家',
			level: 1
		},
		component: (resolve) => {
			require(['../view/chat/index'], resolve)
		}
	},
	{
		name: 'search',
		path: '/search',
		meta: {
			requiresAuth: false,
			title: '搜索',
			level: 0
		},
		component: (resolve) => {
			require(['../view/search/index'], resolve)
		}
	},
	{
		name: 'search-list',
		path: '/search/list',
		meta: {
			requiresAuth: false,
			title: '搜索结果',
			level: 0
		},
		component: (resolve) => {
			require(['../view/search/search-list'], resolve)
		}
	}
]

export const router = new VueRouter({
	mode: 'history',
	routes
})
router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (!isLogin()) {
			next({
				path: '/login',
				query: {redirect: to.fullPath}
			})
		} else {
			next()
		}
	} else {
		next()
	}
})