import {open} from './open'
import {weixin} from './weixin'
import {user} from './user'
import {mall} from './mall'
export const all = {
    initCenterPage(param1,param2,param3){
        return Promise.all([user.userAccountInfo(param1),user.usertrackInfo(param2),mall.hotGoods(param3)]);
    }
}