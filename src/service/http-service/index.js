import {open} from './open'
import {weixin} from './weixin'
import {user} from './user'
import {mall} from './mall'
import {all} from './all'
const combinePost = Object.assign(open, weixin, user, mall);
export const http = combinePost;
export const httpAll = all;