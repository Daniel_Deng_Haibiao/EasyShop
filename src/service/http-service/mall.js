import {post} from "./axios";

export const mall = {
    hotGoods(val) {
        return post({
            type: 'post',
            url: '/api/mall/hotGoods',
            params: val
        })
    },
    category(val) {
        return post({
            type: 'post',
            url: '/api/mall/category',
            params: val
        })
    }
}