import {post} from "./axios";

export const open = {
    getAddressList(val) {
        return post({
            type: 'post',
            url: '/api/open/addressList',
            params: val
        })
    }
}