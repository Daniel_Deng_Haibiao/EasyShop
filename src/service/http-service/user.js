import {post} from "./axios";

export const user = {
    regist(val) {
        return post({
            url: '/api/user/regist',
            params: val
        })
    },
    login(val) {
        return post({
            url: '/api/user/login',
            params: val
        })
    },
    loginByWeixin(val) {
        return post({
            url: '/api/wechat/login',
            params: val
        })
    },
    userInfo(val) {
        return post({
            url: '/api/user/baseInfo',
            params: val
        })
    },
    userAccountInfo(val) {
        return post({
            url: '/api/user/accountInfo',
            params: val
        })
    },
    usertrackInfo(val) {
        return post({
            url: '/api/user/trackInfo',
            params: val
        })
    },
    userEdit(val) {
        return post({
            url: '/api/user/editBaseInfo',
            params: val
        })
    },

    completeInfo(val) {
        return post({
            url: '/api/user/completeBaseInfo',
            params: val
        })
    },
    getUserAddressList(val) {
        return post({
            url: '/api/user/addressList',
            params: val
        })
    },
    addUserAddress(val) {
        return post({
            url: '/api/user/addAddress',
            params: val
        })
    },
    getUserCart(val) {
        return post({
            url: '/api/user/cart',
            params: val
        })
    },
    getUserRecommend(val) {
        return post({
            url: '/api/user/recommend',
            params: val
        })
    },
    gerUserCart(val) {
        return post({
            url: '/api/user/cart',
            params: val
        })
    },
    updateUserCartProductNum(val) {
        return post({
            url: '/api/user/updateCartProductNum',
            params: val
        })
    },
    orderFirmInfo(val) {
        return post({
            url: '/api/user/orderFirmInfo',
            params: val
        })
    },
    orderSubmit(val) {
        return post({
            url: '/api/user/orderSubmit',
            params: val
        })
    },
    orderPayInfo(val) {
        return post({
            url: '/api/user/orderPayInfo',
            params: val
        })
    },
}
