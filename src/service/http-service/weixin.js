import {post} from "./axios";

export const weixin = {
    weixinSignature(val) {
        return post({
            url: '/api/wechat/jsSignature',
            params: val
        })
    }
}