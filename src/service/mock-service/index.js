import Mock from 'mockjs'
import config from '../../config/index'
import response from './response'

/*
* 用户注册
* */
Mock.mock(config.mock.api + '/mobile/user/regist', (option) => {
    return response.regist();
});

/*
* 用户登陆
* */
Mock.mock(config.mock.api + '/mobile/user/login', (option) => {
    return response.login();
});

/*
* 用户信息获取
* */
Mock.mock(config.mock.api + '/mobile/user/userInfo', (option) => {
    return response.userInfo();
});

/*
* 用户信息编辑
* */
Mock.mock(config.mock.api + '/mobile/user/edit', (option) => {
    return response.userEdit();
});