const customizedReturn = function (success, data, msg, code) {
	return {
		success: success,
		data: data,
		msg: msg,
		code: code
	}
}

export default {
	regist () {
		return customizedReturn(true, {
			token: 'token'
		}, '用户注册成功', 10000)
	},
	login () {
		return customizedReturn(true, {
			token: 'token'
		}, '登录成功', 10000)
	},
	userInfo () {
		return customizedReturn(true, {
			nickName: 'Neho',
			realName: '邓海报',
			userAvatar: 'http://p1.music.126.net/yg8uMQJajvbSzqh5ixx7Ng==/19094118928381228.jpg?param=180y180'
		}, '获取用户信息成功', 10000)
	},
	userEdit () {
		return customizedReturn(true, null, '修改用户信息成功', 10000)
	}
}