import {ready} from "./signature";
import conf from "../../config";

export default {
	login () {
		window.location.href = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + conf.wx.appid + "&redirect_uri=" + encodeURIComponent(conf.wx.redirect_uri) + "&response_type=code&scope=" + conf.wx.scope + "&state=STATE#wechat_redirect"
	},
	chooseImage () {
		return new Promise(function (resolve, reject) {
			ready().then(wx => {
				wx.chooseImage({
					count: 1,
					sizeType: ['original', 'compressed'],
					sourceType: ['album', 'camera'],
					success: function (res) {
						resolve(res);
					}
				});
			}, error => {
				reject(error);
			})
		})
	}
}