import {http} from '../http-service';

export const ready = function () {
    return new Promise(function (resolve, reject) {
        http.weixinSignature({url: location.href}).then(res => {
            wx.config(res.data);
            wx.ready(function () {
                resolve(wx);
            });
            wx.error(function (error) {
                reject(error);
            });
        });
    });
};