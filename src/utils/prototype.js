Array.prototype.isSubset = function (arr) {
    for (let i = this.length - 1; i >= 0; i--) {
        if (!arr.includes(this[i])) {
            return false;
        }
    }
    return true;
}