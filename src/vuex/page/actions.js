import * as types from './mutation-types'

export default {
    initCategoryList({commit}, a) {
        commit(types.INIT_CATEGORY_LIST, a);
    },
    selectCategory({commit}, n) {
        commit(types.SELECT_CATEGORY, n);
    }
}