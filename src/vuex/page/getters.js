export default {
    categoryTabList: state => {
        let arrCategory = [];
        state.category.list.forEach(val => {
            if (val.pid === 0) {
                let data = {
                    id: val.id,
                    name: val.name
                };
                arrCategory.push(data);
            }
        });
        return arrCategory;
    },
    activeCategoryId: state => {
        return state.category.active.id;
    },
    activeCategoryData: state => {
        const activeId = state.category.active.id;
        const categoryList = state.category.list;
        let data = {};
        data.banner = {
            imgUrl: state.category.active.bannerUrl
        };
        data.secondList = [];
        categoryList.forEach(val => {
            if (val.pid === activeId) {
                let data1 = val;
                data1.thirdList = [];
                categoryList.forEach(val1 => {
                    if (val1.pid === val.id) {
                        data1.thirdList.push(val1);
                    }
                })
                data.secondList.push(data1);
            }
        });
        return data;
    }
}