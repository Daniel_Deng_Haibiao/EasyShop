import * as types from './mutation-types'

export default {
    [types.INIT_CATEGORY_LIST](state, a) {
        state.category.list = a;
        for (let i = 0; i < state.category.list.length; i++) {
            if (state.category.list[i].pid === 0) {
                state.category.active = state.category.list[i];
                return;
            }
        }
    },
    [types.SELECT_CATEGORY](state, n) {
        for (let i = 0; i < state.category.list.length; i++) {
            if (state.category.list[i].id === n) {
                state.category.active = state.category.list[i];
                return;
            }
        }
    }
}