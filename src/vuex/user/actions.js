import * as types from './mutation-types'

export default {
	initUserInfo ({commit}, o) {
		commit(types.INIT_USER_INFO, o);
	},
	clearUserInfo ({commit}) {
		commit(types.CLEAR_USER_INFO);
	},
	completeUserInfo ({commit}, o) {
		commit(types.COMPLETE_USER_INFO, o);
	},
	updateUserInfo ({commit}, o) {
		commit(types.UPDATE_USER_INFO, o);
	},
	initUserCart ({commit}, a) {
		commit(types.INIT_USER_CART, a);
	},
	clearUserCart ({commit}) {
		commit(types.CLEAR_USER_CART);
	},
	selectCartProduct ({commit}, n) {
		commit(types.SELECT_CART_PRODUCT, n);
	},
	cancelCartProduct ({commit}, n) {
		commit(types.CANCEL_CART_PRODUCT, n);
	},
	selectCartShop ({commit}, n) {
		commit(types.SELECT_CART_SHOP, n);
	},
	cancelCartShop ({commit}, n) {
		commit(types.CANCEL_CART_SHOP, n);
	},
	selectCartAll ({commit}, n) {
		commit(types.SELECT_CART_ALL);
	},
	cancelCartAll ({commit}, n) {
		commit(types.CANCEL_CART_ALL);
	},
	updateCartProductNum ({commit}, o) {
		commit(types.UPDATE_CART_PRODUCT_NUM, o);
	},
	initOrderFirm ({commit}, o) {
		commit(types.INIT_ORDER_FIRM, o);
	}
}