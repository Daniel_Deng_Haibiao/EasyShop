export default {
    totalCartPrice: state => {
        let price = 0;
        state.userCart.selectProduct.forEach((value, index, array) => {
            price = price + value.productPrice * value.productNum;
        });
        return price;
    },
    selectProductIds: state => {
        let arrIds = [];
        state.userCart.selectProduct.forEach((value) => {
            arrIds.push(value.id);

        });
        return arrIds;
    },
    selectShopIds: state => {
        let selectShopIds = [];
        let selectProductIds = [];
        state.userCart.selectProduct.forEach((value) => {
            selectProductIds.push(value.id);

        });
        state.userCart.initCart.forEach((value) => {
            let ids = [];
            for (let i = 0; i < value.products.length; i++) {
                ids.push(value.products[i].id);
            }
            if (ids.isSubset(selectProductIds)) {
                selectShopIds.push(value.shopId);
            }
        });
        return selectShopIds;
    },
    isSelectAll: state => {
        let arrProduct = [];
        let arrSelectProduct = state.userCart.selectProduct;
        state.userCart.initCart.forEach((value) => {
            arrProduct = arrProduct.concat(value.products);
        });
        if (arrProduct.length === arrSelectProduct.length) {
            return true;
        }
        return false;
    },
    selectProductIds: state => {
        let arrIds = [];
        let arrSelectProduct = state.userCart.selectProduct;
        arrSelectProduct.forEach((value) => {
            arrIds.push(value.id);
        });
        return arrIds;
    },
    orderFirmInfo: state => {
        let data = {};
        let imgUrl = [];
        let productNum = 0;
        let totalPrice = 0;
        let strAddress = state.userOrderFirm.address.province_name +
            state.userOrderFirm.address.city_name +
            state.userOrderFirm.address.area_name +
            state.userOrderFirm.address.address;
        data.address = {
            name: state.userOrderFirm.address.receiver,
            phone: state.userOrderFirm.address.phone,
            details: typeof strAddress === 'string' ? strAddress : ''
        };
        state.userOrderFirm.product.forEach((value) => {
            imgUrl.push(value.goodsImg);
            productNum = productNum + value.productNum;
            totalPrice = totalPrice + parseInt(value.price) * value.productNum;
        });
        data.product = {
            imgUrl: imgUrl,
            num: productNum
        };
        data.price = {
            total: totalPrice,
            minus: 0,
            express: 0,
            actual: totalPrice
        };
        return data;
    },
    orderFirmSubmit: state => {
        let field = {};
        let product = [];
        state.userOrderFirm.product.forEach((value) => {
            let data = {
                product_id: value.productId,
                product_num: value.productNum
            };
            product.push(data);
        });
        field.product = product;
        field.shippingId = state.userOrderFirm.address.id;
        field.expressId = 1;
        field.peas = 0;
        field.couponId = '';
        field.payId = 1;
        return field;
    }
}