import * as types from './mutation-types'

export default {
	[types.INIT_USER_INFO] (state, o) {
		state.baseInfo.loginStatus = true;
		state.baseInfo.nickName = o.nickName;
		state.baseInfo.realName = o.realName;
		state.baseInfo.avatar = o.userAvatar;
		state.baseInfo.levelName = o.levelName;
		state.baseInfo.levelValue = o.levelValue;
	},
	[types.CLEAR_USER_INFO] (state) {
		state.baseInfo.loginStatus = false;
		state.baseInfo.nickName = '';
		state.baseInfo.realName = '';
		state.baseInfo.avatar = '';
		state.baseInfo.levelName = '';
		state.baseInfo.levelValue = '';
	},
	[types.COMPLETE_USER_INFO] (state, o) {
		state.baseInfo.nickName = o.nickName;
		state.baseInfo.avatar = o.avatar;
		state.baseInfo.phone = o.phone;
	},
	[types.UPDATE_USER_INFO] (state, o) {
		state.baseInfo[o.name] = o.value;
	},
	[types.INIT_USER_CART] (state, o) {
		state.userCart.initCart = o;
	},
	[types.CLEAR_USER_CART] (state) {
		state.userCart.initCart = [];
		state.userCart.selectProduct = [];
	},
	[types.SELECT_CART_PRODUCT] (state, n) {
		const initCart = state.userCart.initCart;
		for (let i = 0; i < initCart.length; i++) {
			let products = initCart[i].products;
			for (let j = 0; j < products.length; j++) {
				if (initCart[i].products[j].id === n) {
					state.userCart.selectProduct.push(initCart[i].products[j]);
					return;
				}
			}
		}
	},
	[types.CANCEL_CART_PRODUCT] (state, n) {
		const selectProduct = state.userCart.selectProduct;
		for (let i = 0; i < selectProduct.length; i++) {
			if (selectProduct[i].id === n) {
				selectProduct.splice(i, 1);
				return;
			}
		}
	},
	[types.SELECT_CART_SHOP] (state, n) {
		const initCart = state.userCart.initCart;
		for (let i = 0; i < initCart.length; i++) {
			if (initCart[i].shopId === n) {
				state.userCart.selectProduct = state.userCart.selectProduct.concat(initCart[i].products);
				return;
			}
		}
	},
	[types.CANCEL_CART_SHOP] (state, n) {
		const initCart = state.userCart.initCart;
		for (let i = 0; i < initCart.length; i++) {
			if (initCart[i].shopId === n) {
				let selectProduct = state.userCart.selectProduct;
				selectProduct.forEach((value, index, array) => {
					if (initCart[i].products.includes(value)) {
						array.splice(index, 1);
					}
				});
				return;
			}
		}
	},
	[types.SELECT_CART_ALL] (state) {
		state.userCart.selectProduct = [];
		const initCart = state.userCart.initCart;
		for (let i = 0; i < initCart.length; i++) {
			initCart[i].products.forEach((value, index, array) => {
				state.userCart.selectProduct.push(value);
			});
		}
	},
	[types.CANCEL_CART_ALL] (state) {
		state.userCart.selectProduct = [];
	},
	[types.UPDATE_CART_PRODUCT_NUM] (state, o) {
		const initCart = state.userCart.initCart;
		const selectProduct = state.userCart.selectProduct;
		for (let i = 0; i < initCart.length; i++) {
			let products = initCart[i].products;
			for (let j = 0; j < products.length; j++) {
				if (initCart[i].products[j].id === o.id) {
					initCart[i].products[j].productNum = initCart[i].products[j].productNum + o.num;
					return;
				}
			}
		}
		for (let i = 0; i < selectProduct.length; i++) {
			if (selectProduct[i].id === o.id) {
				selectProduct[i].productNum = selectProduct[i].productNum + o.num;
				return;
			}
		}
	},
	[types.INIT_ORDER_FIRM] (state, o) {
		state.userOrderFirm.address = o.address;
		state.userOrderFirm.product = o.product;
		state.userOrderFirm.payType = o.payType;
		state.userOrderFirm.express = o.express;
	},
}