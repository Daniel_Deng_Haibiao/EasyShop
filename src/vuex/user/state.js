export default {
    baseInfo: {
        loginStatus: false,
        nickName: '',
        realName: '',
        avatar: '',
        levelName: '',
        levelValue: '',
        phone: ''
    },
    userCart: {
        initCart: [],
        selectProduct: []
    },
    userOrderFirm: {
        address: '',
        product: [],
        payType: '',
        express: '',
        coupon: '',
        peas: '',
        selectCoupon: '',
        selectPeas: ''
    }
}